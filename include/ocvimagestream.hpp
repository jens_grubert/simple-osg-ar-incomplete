#pragma once
#include "include/trackercallbackmtupdate.hpp"
#include <osg/ImageStream>
//#include <sstt/globals.hpp>
#include <OpenThreads/Thread>
#include <opencv2/opencv.hpp>


class TrackerCallbackMTUpdate;
// the ImageStream converts a opencv image into an osg::Image
class OCVImageStream : public osg::ImageStream {
	static void ConvertCVMatToOSGImage(const cv::Mat & input, osg::Image& out);

public:
	
	OCVImageStream(const cv::Mat & cvImage);
	//OCVImageStream();
	//void init(const cv::Mat & cvImage);
	void initWithCapture(cv::Ptr<cv::VideoCapture> capture);
	void update(osg::NodeVisitor *nv);
	bool requiresUpdateCall() const { return true; }
	void setDrawTargetOutline(bool b) { _drawTargetOutline = b; }
	//void (TrackerCallbackMTUpdate::*_fPtr) (cv::Mat &);
	void setTrackerCallback(osg::ref_ptr<TrackerCallbackMTUpdate>tracker) { _tracker = tracker; }

protected:
	virtual ~OCVImageStream();	
	const cv::Mat & _cvImage;
	cv::Ptr<cv::VideoCapture> _capture;

	bool _useCapture;

	bool _drawTargetOutline;

	osg::ref_ptr<TrackerCallbackMTUpdate> _tracker;

};
