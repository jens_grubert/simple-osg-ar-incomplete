#pragma once

#include <iostream>

#include <osg/MatrixTransform>
#include <osg/Node>

#include <osg/observer_ptr>
#include <osg/NodeCallback>
#include <osgWidget/Label>

#include "include/markerDetectorBW.hpp"


/**
A node callback which calls a tracker and pipes the tracking result into a PositionAttitudeTransform (PAT) transform
*/
class TrackerCallbackMTUpdate : public osg::NodeCallback
{
public:
	osg::observer_ptr<osg::MatrixTransform> _transform; // the OSG node that should get the pose assigned from the marker tracker
	
	cv::Ptr<MarkerDetectorBW> _tracker; // reference to initialized marker tracker
	//cv::Ptr<cv::VideoCapture> _vc; // reference to initialized video capture device
	TrackerCallbackMTUpdate();	
	virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);		
	void setCameraOffset(float x, float y, float z);
	void getCameraOffset(float &x, float &y, float &z);

	void setDrawTargetOutline(bool b) { _drawTargetOutline = b; }

protected:
	
	// to compensate for non centered placements of the tracking camera
	float _cameraOffsetX;
	float _cameraOffsetY;
	float _cameraOffsetZ;

	bool _drawTargetOutline;
};
