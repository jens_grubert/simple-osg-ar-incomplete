
#include "include/pose.hpp"

#include <sstream>
using namespace Eigen;
using namespace cv;

/*
OSG / OpenGL
virtual camera + screen coordinate system
|----------------------------------------|
|    y									 |
|   ^									 |
|   |									 |
|   |									 |
|   |__________> x						 |
|   \									 |
|    \									 |
|     \									 |
|      V  z                   screen     |
|----------------------------------------|

OpenCV Tracker coordinate system
|----------------------------------------|
|    z									 |
|  ^									 |
|   \  									 |
|    \ 									 |
|     \__________> x					 |
|     |									 |
|     |								     |
|     |									 |
|     V  y								 |
|                         screen		 |
|----------------------------------------|
*/

cv::Mat Pose::translation()
{
	return _translation;
}

void Pose::setTranslation(const cv::Mat &translation)
{
	_translation = translation;
	_isEmpty = false;
}

cv::Mat Pose::rotation() const
{
	return _rotation;
}

void Pose::setRotation(const cv::Mat &rotation)
{
	_rotation = rotation;
	_isEmpty = false;
}

Pose::Pose()
{
	_isEmpty = true;
}

double Pose::score() const
{
    return _score;
}

void Pose::setScore(double score)
{
    _score = score;
}

double Pose::lastTimeSeen() const
{
    return _lastTimeSeen;
}

void Pose::setLastTimeSeen(double lastTimeSeen)
{
    _lastTimeSeen = lastTimeSeen;
}


void Pose::modelViewMatrix(cv::Mat &mv) const
{
	// Rt part
	cv::Mat Rt = cv::Mat::eye(4, 4, cv::DataType<double>::type);

	cv::Mat Rm, T;

	T = _translation;
	cv::Rodrigues(_rotation, Rm);
	
	// copy the pose
	for (int r = Rm.rows; r-->0;) {
		for (int c = Rm.cols; c-->0;)
			Rt.at<double>(r, c) = Rm.at<float>(r, c);
		Rt.at<double>(r, 3) = T.at<float>(r, 0);
	}

	//! @todo combine rotation and flip
	// OpenGL has reversed Y & Z coords
	cv::Mat reverseYZ = cv::Mat::eye(4, 4, cv::DataType<double>::type);
	reverseYZ.at<double>(1, 1) = reverseYZ.at<double>(2, 2) = -1;

	// rotate around X to make Y up on target
	cv::Mat Yup = cv::Mat::zeros(4, 4, cv::DataType<double>::type);
	Yup.at<double>(0, 0) = Yup.at<double>(3, 3) = 1;
	Yup.at<double>(2, 1) = -1;
	Yup.at<double>(1, 2) = 1;

	// chain them up
	mv = reverseYZ * Rt * Yup;

	// transpose
	mv = mv.t();




}


template <typename T_> 
void 
Pose::rodriguesToQuaternion(const cv::Mat rodrigues, Eigen::Quaternion<T_>& qtx)
{
	T_ theta = std::sqrt(rodrigues.at<T_>(0) * rodrigues.at<T_>(0) +
		rodrigues.at<T_>(1) * rodrigues.at<T_>(1) +
		rodrigues.at<T_>(2) * rodrigues.at<T_>(2));
	qtx = Eigen::Quaternion<T_>(Eigen::AngleAxis<T_>(theta,
		Eigen::Matrix<T_, 3, 1>(rodrigues.at<T_>(0) / theta, rodrigues.at<T_>(1) / theta, rodrigues.at<T_>(2) / theta)));
}
