
#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "include/markerDetectorBW.hpp"
#include "include/markerBW.hpp"
#include "include/ocvimagestream.hpp"

#include <osg/Matrix>
#include <osg/io_utils>
#include <osg/ImageStream>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>
#include <osg/Material>
#include <osg/BlendFunc>
#include <osg/ShapeDrawable>
#include <osg/TexGenNode>
#include <osg/TexMat>

#include <osg/OperationThread>

#include <osgDB/ReadFile>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

#include <osgUtil/Optimizer>


#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <osgGA/GUIEventHandler>


#include <osgWidget/Util>
#include <osgWidget/WindowManager>
#include <osgWidget/ViewerEventHandlers>
#include <osgWidget/Table>
#include <osgWidget/Box>
#include <osgWidget/Label>

using namespace cv;
using namespace std;
using namespace osg;

/*
OSG / OpenGL
virtual camera + screen coordinate system
|----------------------------------------|
|    y									 |
|   ^									 |
|   |									 |
|   |									 |
|   |__________> x						 |
|   \									 |
|    \									 |
|     \									 |
|      V  z                   screen     |
|----------------------------------------|

OpenCV Tracker coordinate system
|----------------------------------------|
|    z									 |
|  ^									 |
|   \  									 |
|    \ 									 |
|     \__________> x					 |
|     |									 |
|     |								     |
|     |									 |
|     V  y								 |
|                         screen		 |
|----------------------------------------|
*/

osg::ref_ptr<osg::Group> _root;
osg::ref_ptr<osg::Camera> _mainCamera;
osg::ref_ptr<osg::MatrixTransform> _trackerTransform; //_screenTargetTransform
osg::ref_ptr<TrackerCallbackMTUpdate> _screenTCB;
osg::ref_ptr<osgViewer::Viewer> _viewer; /// the osg viewer

cv::Ptr<cv::VideoCapture> _videoCapture;
Mat _videoImage;
osg::ref_ptr<OCVImageStream> _videoStream;
cv::Ptr<MarkerDetectorBW>  _md;

osg::ref_ptr<osg::Node> create3DScene() {
	osg::ref_ptr<osg::Group> sceneRoot = new osg::Group;
	osg::ref_ptr<osg::Geode> boxGeode = new osg::Geode();
	osg::ref_ptr<osg::Box> box = new osg::Box(osg::Vec3(0, 0, 0), 9.2f);
	osg::ref_ptr<osg::ShapeDrawable> boxDrawable = new osg::ShapeDrawable(box);
	boxGeode->addDrawable(boxDrawable);
	sceneRoot->addChild(boxGeode);
	return sceneRoot;
}

osg::ref_ptr<osg::Node> createVideoBackground(osg::Image* image, float width, float height, bool flipY)
{
	// set up the ortohographic camera
	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	
	camera = new osg::Camera();		

	// STUDENT TODO: setup an orthographic camera for video background rendering
	
	
	camera->setViewport(0, 0, width, height);
	camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);	
	camera->setRenderOrder(osg::Camera::NESTED_RENDER);	
	camera->setViewMatrix(osg::Matrix::identity());
    camera->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);
	////// only clear the depth buffer
	camera->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera->getOrCreateStateSet()->setMode(GL_LIGHTING, GL_FALSE);
	camera->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, GL_FALSE);	
	
	// add the video image
	osg::Geode* video_geode = new osg::Geode; 
	osg::StateSet* stateset = video_geode->getOrCreateStateSet();
	stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);	
	osg::Texture2D* texture;
	osg::ref_ptr<osg::ImageStream> image_stream = dynamic_cast<osg::ImageStream*>(image);
	
	// we actually have an image
	if (image_stream == 0) {
		texture = new osg::Texture2D(image);
	}
	else { // we have an imagestream
		image->setPixelBufferObject(new osg::PixelBufferObject(image_stream.get()));
		texture = new osg::Texture2D(image_stream.get());
	}
	
	// assign the image stream to the texture
	//osg::Texture2D* texture = new osg::Texture2D(image);
	texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
	texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
	texture->setResizeNonPowerOfTwoHint(false);	
	osg::Geometry* pictureQuad;

	if (flipY)
	{
		pictureQuad = osg::createTexturedQuadGeometry(osg::Vec3(0, 0 + height, 0.0), osg::Vec3(width, 0.0f, 0.0), osg::Vec3(0.0f, -height, 0.0f));		
	}
	else {
		pictureQuad = osg::createTexturedQuadGeometry(osg::Vec3(0, 0, 0.0), osg::Vec3(width, 0.0f, 0.0), osg::Vec3(0.0f, height, 0.0f));
	}	
	
	pictureQuad->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	video_geode->addDrawable(pictureQuad);
	osg::ref_ptr<osg::PositionAttitudeTransform> sceneAdjust = new osg::PositionAttitudeTransform();
	sceneAdjust->addChild(video_geode);	
	sceneAdjust->setDataVariance(osg::Object::DYNAMIC);
	camera->addChild(sceneAdjust);
	return camera;
}



void setupOpenSceneGraph() {	
	_viewer = new osgViewer::Viewer();	
	_root = new osg::Group();
	// parameterize and start the viewer	
	_viewer->getCamera()->setClearColor(osg::Vec4(0.9, 0.3, 0.3, 1));
	_viewer->getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);

	// STUDENT TODO: set the correct perspective projection parameters to fit the field of view of the real camera
	_viewer->getCamera()->setProjectionMatrixAsPerspective(80, 640.0 / 480.0, 0.001, 10000.0);	

	_viewer->setSceneData(_root.get());
	_viewer->setUpViewInWindow(50, 50, 640, 480);

		// create the videobackground
    _videoStream = new OCVImageStream(_videoImage);
	_videoStream->initWithCapture(_videoCapture);
	// add 3D scene + video background to camera and then the root node
    _root->addChild(createVideoBackground(_videoStream, 640, 480, true));
	
	// create the 3D scene
	// the transform node holding the scene which should be displayed registered
	_trackerTransform = new osg::MatrixTransform();
	_trackerTransform->addChild(create3DScene());
	// set the inital position of the 3D scene
	osg::Matrixd transM;
	transM.makeTranslate(0, 0, -30);
	_trackerTransform->setMatrix(transM);

	// a callback which ensures, that the transform is updated with the tracking data 
	_screenTCB = new TrackerCallbackMTUpdate();
	_screenTCB->_transform = _trackerTransform;
	_screenTCB->_tracker = _md;
	_trackerTransform->addEventCallback(_screenTCB);
	_root->addChild(_trackerTransform);

    // 	double fovy = -1, aspectRatio = -1, zNear = -1, zFar = -1;
	//_viewer->getCamera()->getProjectionMatrixAsPerspective(fovy, aspectRatio, zNear, zFar);
	//cout << "fovy: " << fovy << " aspectRatio: " << aspectRatio << " zNear: " << zNear << " zFar: " << zFar << endl;
	_viewer->realize();
}

int main(int argc, char** argv) {

	cout << "command line arguments: " << endl;
	// for now hardcode the parameters
	for (int i = 0; i < argc; i++) {
		cout << "i " << i << ": " <<  argv[i] << endl;

	}
	if (argc == 4) {
		FileStorage fs2(argv[2], FileStorage::READ);
		//FileStorage fs2("camera.yml", FileStorage::READ);
		// get calibration data

		Mat cameraMatrix, distCoeffs;
		fs2["camera_matrix"] >> cameraMatrix;
		fs2["distortion_coefficients"] >> distCoeffs;

		cout << "intrinsics: " << cameraMatrix << endl;
		cout << "distCoeffs: " << distCoeffs << endl;

		_videoCapture = new VideoCapture();
		
		int cameraId = (int)strtol(argv[1], NULL, 10);
		cout << "cameraId: " << cameraId << endl;;
		_videoCapture->open(cameraId);
		_videoCapture->set(CV_CAP_PROP_FRAME_WIDTH, 640);
		_videoCapture->set(CV_CAP_PROP_FRAME_HEIGHT, 480);
		_videoCapture->set(CV_CAP_PROP_FPS, 30);
		if (!_videoCapture->isOpened()) {
			cerr << "Could not open capture device " << (int)argv[1] << endl;
			return -1;
		}

		float markerSize = strtof(argv[3], NULL);
		cout << "marker size: " << markerSize << endl;

		// specify marker size in the same units as used for the camera calibration
		_md = new MarkerDetectorBW(cameraMatrix, distCoeffs, markerSize, markerSize);
		cv::namedWindow("VideoStream", CV_WINDOW_NORMAL);
		
		setupOpenSceneGraph();

		bool quit = false;
		while (!_viewer->done() && !quit)
		{	
			bool bSuccess = _videoCapture->read(_videoImage); // read a new frame from video
			if (!bSuccess) //if not success, break loop
			{
				cout << "Cannot read a frame from video stream" << endl;
				//break;
				continue;
			}
			_md->processFrame(_videoImage);
			vector<Pose> poses = _md->getPoses();
			if (poses.size() == 1) {

				stringstream transSS, rotSS;
				transSS << "trans (x,y,z): " << poses.at(0).translation().at<float>(0) << " " << poses.at(0).translation().at<float>(1) << " " << poses.at(0).translation().at<float>(2);
				cv::putText(_videoImage, transSS.str(), cv::Point(10, 30), CV_FONT_NORMAL, 0.7, cv::Scalar(0, 255, 0), 1);

				rotSS << "rot (rodrigues): " << poses.at(0).rotation().at<float>(0) << " " << poses.at(0).rotation().at<float>(1) << " " << poses.at(0).rotation().at<float>(2);
				cv::putText(_videoImage, rotSS.str(), cv::Point(10, 60), CV_FONT_NORMAL, 0.7, cv::Scalar(0, 255, 0), 1);
			}
			imshow("VideoStream", _videoImage);


			if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
			{
				cout << "esc key is pressed by user" << endl;
				quit = true;
			}

			// render step
			_viewer->advance();			
			_viewer->eventTraversal();
			_viewer->updateTraversal();			
			_viewer->renderingTraversals();			
		}		
		
		_videoCapture->release();
	}
	else {
		cout << "please specify a camera id, a calibration file and the marker size: e.g. \"0 calib.yml 8.3\"";
	}
}