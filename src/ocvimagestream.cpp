#include "include/ocvimagestream.hpp"

#include "osg/Image"
#include "osg/ImageStream"
using namespace std;

OCVImageStream::OCVImageStream(const cv::Mat & cvImage)
: osg::ImageStream()
, _cvImage(cvImage),
_useCapture(false)
	{
	_drawTargetOutline = false;
	}

OCVImageStream::~OCVImageStream()
{
	quit(true);
}

void OCVImageStream::initWithCapture(cv::Ptr<cv::VideoCapture> capture)
{
	_capture = capture;
	_useCapture = true;
}


void OCVImageStream::update( osg::NodeVisitor* nv )
{
	if (_useCapture) 
	{

		cv::Mat frame;
		if (_capture->retrieve((frame))) 
		{

		/*	if (_drawTargetOutline) {
				if (_tracker != 0) {
					_tracker->drawNFTTargetPose(frame);
				}
			}*/
			OCVImageStream::ConvertCVMatToOSGImage(frame, *this);
		}
	}
	else {
		OCVImageStream::ConvertCVMatToOSGImage(_cvImage, *this);
	}
	
}

void OCVImageStream::ConvertCVMatToOSGImage(const cv::Mat & input, osg::Image& out)
{
	// wild guess: we might not have an image yet
	if (out.s() != input.cols || out.t() != input.rows)
	{
		out.allocateImage(input.cols,
						  input.rows, 1,
						  GL_RGB, GL_UNSIGNED_BYTE, 1);
		
		// set format - OpenCV always BGR
		out.setPixelFormat(GL_BGR);
	}
	
	// if data is continous we can just copy it
	if (input.isContinuous()) {
		memcpy(out.data(),input.data,out.getImageSizeInBytes());
	} else {
		// however sometime some aligments need to be accounted for
		for (register int r = 0; r < input.rows; ++r)
			for (register int c = 0; c < input.cols; ++c) {
				*out.data(c,r) = input.at<uchar>(r,c);
			}
			
	}

	
	// need to use dirty() as setModifiedCount(int) does not update
	// the backend buffer object
	out.dirty();
	
}