#include "include/trackercallbackmtupdate.hpp"
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>

using namespace std;
TrackerCallbackMTUpdate::TrackerCallbackMTUpdate() : osg::NodeCallback()
{
	_cameraOffsetX = 0;
	_cameraOffsetY = 0;
	_cameraOffsetZ = 0;
	_drawTargetOutline = false;
}
void TrackerCallbackMTUpdate::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
	
	vector<Pose> poses = _tracker->getPoses();
	if (poses.size() == 1 && poses.at(0).isEmpty() == false) {
		cv::Mat mvmCV(4, 4, cv::DataType<double>::type);
		poses.at(0).modelViewMatrix(mvmCV);
		osg::Matrixd mvm;
		mvm.set(&mvmCV.at<double>(0));
		_transform->setMatrix(mvm);
	}
				
		this->traverse(node,nv);
}


void TrackerCallbackMTUpdate::setCameraOffset(float x, float y, float z) {
	_cameraOffsetX = x;
	_cameraOffsetY = y;
	_cameraOffsetZ = z;
}


void TrackerCallbackMTUpdate::getCameraOffset(float &x, float &y, float &z)
{
	x = _cameraOffsetX;
	y = _cameraOffsetY;
	z = _cameraOffsetZ;

}
